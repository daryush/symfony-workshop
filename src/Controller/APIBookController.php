<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Book;
use App\Form\Type\BookType;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/book", name="api_book_")
 */
class APIBookController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(
        BookRepository $bookRepository,
        SerializerInterface $serializer
    ) {
        $customContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];

        return new JsonResponse(
            $serializer->serialize(
                $bookRepository->findAll(),
                'json',
                $customContext
            )
        );
    }

    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request)
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            return new JsonResponse($book);
        }

        return new JsonResponse($form->getErrors(), 400);
    }
}
