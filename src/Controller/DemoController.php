<?php

namespace App\Controller;

use App\Entity\DemoObject;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/demo")
 */
class DemoController extends AbstractController
{
    /**
     * @Route("/{name}", name="demo_index")
     */
    public function index($name)
    {
        $nameObject = new DemoObject($name);
        return $this->render('demo/index.html.twig', [
            'controller_name' => 'DemoController',
            'nameObject' => $nameObject
        ]);
    }
}
