<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AuthorFixtures extends Fixture
{
    const AUTHOR_JAN = 'author-jan';
    const AUTHOR_ROMAN = 'author-roman';

    public function load(ObjectManager $manager)
    {
        $authorJan = (new Author())
            ->setFirstName('Jan')
            ->setLastName('Kowalski')
            ->setAddress(
                (new Address())
                ->setCity('Warszawa')
                ->setStreet('Nowogrodzka')
                ->setZipCode('00-100')
            );

        $authorRoman = (new Author())
            ->setFirstName('Roman')
            ->setLastName('Nowak')
            ->setAddress(
                (new Address())
                ->setCity('Łodź')
                ->setStreet('Piotrkowska')
                ->setZipCode('03-300')
            );

        $manager->persist($authorJan);
        $manager->persist($authorRoman);
        $manager->flush();

        $this->addReference(self::AUTHOR_JAN, $authorJan);
        $this->addReference(self::AUTHOR_ROMAN, $authorRoman);
    }
}
