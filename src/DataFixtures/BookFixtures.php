<?php

namespace App\DataFixtures;

use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;

class BookFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $bookPHP6 = (new Book())
            ->setTitle('PHP6')
            ->setIsbn('33832193783')
            ->setRecommended(true)
            ->setAvailableCopiesNumber(4)
            ->setAuthor(
                $this->getReference(AuthorFixtures::AUTHOR_ROMAN)
            );

        $bookAngular = (new Book())
            ->setTitle('Angular')
            ->setIsbn('33832193782')
            ->setRecommended(false)
            ->setAvailableCopiesNumber(8)
            ->setAuthor(
                $this->getReference(AuthorFixtures::AUTHOR_JAN)
            );

        $bookReact = (new Book())
            ->setTitle('React')
            ->setIsbn('33832193781')
            ->setRecommended(true)
            ->setAvailableCopiesNumber(3)
            ->setAuthor(
                $this->getReference(AuthorFixtures::AUTHOR_ROMAN)
            )
            ->setSimilarBooks(
                new ArrayCollection([$bookAngular])
            );

        $manager->persist($bookPHP6);
        $manager->persist($bookAngular);
        $manager->persist($bookReact);
        $manager->flush();
    }
}
