<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {


        $admin = (new User())
            ->setEmail('admin@test.com')
            ->setFirstName('Admin')
            ->setLastName('Admin')
            ->setRoles(['ROLE_ADMIN'])
        ;
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
             'pass'
        ));
        /** @var Author $author */
        $author = $this->getReference(AuthorFixtures::AUTHOR_ROMAN);
        $authorUser = (new User())
            ->setEmail('author@test.com')
            ->setFirstName($author->getFirstName())
            ->setLastName($author->getLastName())
            ->setRoles(['ROLE_USER'])
        ;
        $authorUser->setPassword($this->passwordEncoder->encodePassword(
            $authorUser,
            'pass'
        ));
        $user = (new User())
            ->setEmail('user@test.com')
            ->setFirstName('User')
            ->setLastName('Normal')
            ->setRoles(['ROLE_USER'])
        ;
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'pass'
        ));
        $vip = (new User())
            ->setEmail('vip@test.com')
            ->setFirstName('User')
            ->setLastName('Normal')
            ->setRoles(['ROLE_USER_VIP'])
        ;
        $vip->setPassword($this->passwordEncoder->encodePassword(
            $vip,
            'pass'
        ));

        $manager->persist($admin);
        $manager->persist($authorUser);
        $manager->persist($user);
        $manager->persist($vip);

        $manager->flush();
    }
}
