<?php
/**
 * Created by PhpStorm.
 * User: daryush
 * Date: 2019-11-27
 * Time: 10:57
 */

namespace App\Entity;

class DemoObject
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
