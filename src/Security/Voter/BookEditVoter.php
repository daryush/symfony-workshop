<?php

namespace App\Security\Voter;

use App\Entity\Book;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class BookEditVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['BOOK_EDIT'])
            && $subject instanceof Book;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var Book $subject */
        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'BOOK_EDIT':
                return in_array('ROLE_ADMIN', $user->getRoles())
                    || (
                        $user->getLastName() === $subject->getAuthor()->getLastName()
                        && $user->getFirstName() === $subject->getAuthor()->getFirstName()
                    );
                break;
        }

        return false;
    }
}
